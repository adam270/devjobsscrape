import * as request from 'request';
import * as cheerio from 'cheerio';

const websites: string[] = [
  'https://careers.stackoverflow.com/jobs?sort=i&q=software+developer',
  'https://www.indeed.com/jobs?q=software+developer&l=',
  'https://www.glassdoor.com/Job/jobs.htm?sc.keyword=software+developer',
  'https://www.linkedin.com/jobs/search?keywords=software%20developer&location=',
  'https://www.dice.com/jobs/q-software_developer-l-',
];

interface Job {
  title: string;
  company: string;
  location: string;
  date: string;
  link: string;
}

// Create an array to store the job objects
const techJobs: Job[] = [];

// Iterate over the websites
websites.forEach((website) => {
  // Make a request to the website
  request(website, (error, response, html) => {
    // Load the HTML into cheerio
    const $ = cheerio.load(html);

    // Select the job listings using a CSS selector (this will be different for each website)
    const jobListings = $('.job-listing');

    // Iterate over the job listings
    jobListings.each((index, element) => {
      // Select the title, company, location, date, and link using CSS selectors (these will also be different for each website)
      const title = $(element).find('.title').text().trim();
      const company = $(element).find('.company').text().trim();
      const location = $(element).find('.location').text().trim();
      const date = $(element).find('.date').text().trim();
      const link = $(element).find('.link').attr('href');

      // Create a job object
      const job = {
        title: title,
        company: company,
        location: location,
        date: date,
        link: link,
      };

      // Push the job object to the techJobs array
      techJobs.push(job);
    });
  });
});

// Output the techJobs array after the scraping is complete
console.log(techJobs);
